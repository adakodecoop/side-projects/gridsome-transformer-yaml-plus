# gridsome-transformer-yaml-plus

> Augmented YAML transformer for Gridsome.

This transformer parses yaml documents and applies selected transformations:

- images: prepends media root folder path to designated image fields
- markdown: transform designated markdown fields to its html representation

If transformations are not configured, it works as the regular [@gridsome/transformer-yaml](https://gridsome.org/plugins/@gridsome/transformer-yaml).

## Installation

- `yarn add gridsome-transformer-yaml-plus`
- `npm install gridsome-transformer-yaml-plus`

## Usage

After installing the package as a development dependency, you can configure the transformer globally
and/or for a specific filesystem source.

```js
// gridsome.config.js

module.exports = {
  transformers: {
    yamlPlus: {
      // Global config
      images: {
        fieldName: 'image',
        mediaPath: '../../static'
      },
      markdown: {
        fieldName: 'text',
      }
    }
  },
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/home/*.yml',
        typeName: 'HomePageContent',
        yamlPlus: {
          // Local config
          images: {
            mediaPath: '../../../static',
          },
          markdown: {
            fieldName: 'body',
          }
        }
      }
    }
  ]
}
```

### Options

```js
yamlPlus: {
  images: {
    fieldName: 'name of the image fields to be transformed',
    mediaPath: 'path to the media root folder relative to src/pages (where graphql queries happen)'
  },
  markdown: {
    fieldName: 'name of the markdown fields to be transformed'
  }
}
```

## Contributors

Designed, developed and maintained by

<!-- prettier-ignore-start -->

| [<img alt="laklau" src="https://secure.gravatar.com/avatar/15c5fa5af66362d8b756c2458a06821c?s=117&d=identicon" width="117">](https://gitlab.com/adakodecoop/web/arcadiacoop.org/graphs/master) | [<img alt="zuzudev" src="https://secure.gravatar.com/avatar/f925124b0eed1eeab8513016914b9150?s=117&d=identicon" width="117">](https://gitlab.com/adakodecoop/web/arcadiacoop.org/graphs/master) |
| :---: | :---: |
| [Klaudia Alvarez](https://github.com/laklau) | [Carles Muiños](https://github.com/zuzust) |

<!-- prettier-ignore-end -->

## Contact

Email: hola[@]adakode[.]org  
Twitter: [@adakodecoop](https://twitter.com/adakodecoop)  
Facebook: [adakodecoop](https://www.facebook.com/adakodecoop)  
LinkedIn: [adakodecoop](https://www.linkedin.com/company/adakodecoop)

## License

The code is &copy; 2019-present [Adakode](https://adakode.org) under the terms of the [Hippocratic License](https://firstdonoharm.dev/version/1/1/license.html).  
