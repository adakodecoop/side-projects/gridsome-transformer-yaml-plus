// @ts-nocheck
const { flatten, unflatten } = require('flat');
const defaultsDeep = require('lodash.defaultsdeep');
const jsYaml = require('js-yaml');
const marked = require('marked');

class YamlPlusTransformer {
  static mimeTypes() {
    return ['text/yaml'];
  }

  constructor(options, { localOptions }) {
    const { images, markdown } = defaultsDeep(localOptions, options);

    this.transforms = { images, markdown };
  }

  parse(content) {
    let data = jsYaml.load(content);

    try {
      let flatData = flatten(data);

      if (this.transforms.images) {
        flatData = this._transformImages(flatData);
      }
      if (this.transforms.markdown) {
        flatData = this._transformMarkdown(flatData);
      }

      data = unflatten(flatData);
    } catch (error) {
      console.log('[YamlPlusTransformer]', error.message);
    } finally {
      return typeof data !== 'object' || Array.isArray(data) ? { data } : data;
    }
  }

  _transformImages(data) {
    const { fieldName, mediaPath } = this.transforms.images;

    if (!fieldName) {
      throw new Error('[images] fieldName required!');
    }
    if (!mediaPath) {
      throw new Error('[images] mediaPath required!');
    }

    const imgRegex = RegExp(`\.${fieldName}$`);

    Object.keys(data)
      .filter(key => imgRegex.test(key))
      .forEach(imgKey => (data[imgKey] = `${mediaPath}${data[imgKey]}`));

    return data;
  }

  _transformMarkdown(data) {
    const { fieldName } = this.transforms.markdown;

    if (!fieldName) {
      throw new Error('[markdown] fieldName required!');
    }

    const mdRegex = RegExp(`\.${fieldName}$`);

    Object.keys(data)
      .filter(key => mdRegex.test(key))
      .forEach(mdKey => (data[mdKey] = marked(data[mdKey])));

    return data;
  }
}

module.exports = YamlPlusTransformer;
